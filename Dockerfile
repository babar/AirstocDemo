FROM ruby:2.3
MAINTAINER Babar Al-Amin <knock@babar.im>

RUN apt-get update -qq && apt-get install -q -y build-essential libpq-dev nodejs

RUN mkdir /app
WORKDIR /app

ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock
RUN bundle install

ADD . /app
