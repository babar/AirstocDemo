# Airstoc Demo
A Dockerized Rails app for Airstock demo.

### Requirements to run the app
* Docker
* docker-compose

### Tools included
* Rails 5
* PostgreSQL
* Sidekiq
* Redis
* NginX

### Run it
* Pull the app locally with git (`git clone git@gitlab.com:babar/AirstocDemo.git`)
* Create the DB: `docker-compose run app rails db:create`
* Migrate: `docker-compose run app rails db:migrate`
* Make sure you don't have anything running on port `80` on your host machine.
* Run `docker-compose up`
Hit http://localhost and You should be good to go.
Also there's a CRUD scaffold available at http://localhost/posts :)
